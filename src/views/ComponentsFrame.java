package views;

import java.awt.Button;
import java.awt.Canvas;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.Scrollbar;
import java.awt.TextArea;
import java.awt.TextField;

import views.components.BorderPanel;

public class ComponentsFrame extends WFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5475809701447035232L;
	private Label label1, label2, label3, label4;
	private TextField textField1, textField2;
	private TextArea textArea1;
	private Panel panel1;
	private Checkbox checkbox1,checkbox2, checkbox3,checkbox4,checkbox5, checkbox6;
	private BorderPanel panel2;
	private CheckboxGroup group1;
	private Choice choice1;
	private List list1;
	private Scrollbar scrollbar1, scrollbar2;
	private Canvas canvas1;
	private Button button1, button2;
	

	
	public ComponentsFrame(MainFrame intance) {
		super(intance);
		initComponents();
	}

	public void initComponents() {
		setSize(500,600);
		setTitle("Componentes");
		setLayout(null);
		
		label1 = new Label("Etiqueta 1");
		label1.setBounds(20, 50, 100, 32);
		add(label1);
		
		label2 = new Label("Etiqueta 2");
		label2.setBounds(20,85,100,32);
		add(label2);
		
		textField1 = new TextField();
		textField1.setBounds(122, 50, 250, 32);
		add(textField1);
		
		textField2 = new TextField();
		textField2.setBounds(122,85,250,32);
		textField2.setEnabled(false);
		add(textField2);
		
		label3 = new Label("Descripción");
		label3.setBounds(20,120,100,32);
		add(label3);
		
		textArea1 = new TextArea();
		textArea1.setBounds(20,155,350,90);		
		add(textArea1);
		
		panel1 = new Panel();
		panel1.setBounds(375, 50, 100, 90);
		panel1.setBackground(Color.LIGHT_GRAY);
		add(panel1);
		
		checkbox1 = new Checkbox("Opción 1");
		checkbox2 = new Checkbox("Opción 2");
		checkbox3 = new Checkbox("Opción 3");
		
		panel1.add(checkbox1);
		panel1.add(checkbox2);
		panel1.add(checkbox3);
		
		panel2= new BorderPanel();
		panel2.setBorderVisible(true);
		panel2.setBounds(375,155,100,90);		
		add(panel2);
		
		group1 = new CheckboxGroup();
		checkbox4 = new Checkbox("Opción A", group1, true);
		checkbox5 = new Checkbox("Opción B", group1, false);
		checkbox6 = new Checkbox("Opción C", group1, false);
		
		panel2.add(checkbox4);
		panel2.add(checkbox5);
		panel2.add(checkbox6);
		
		label4 = new Label("Selecciona");
		label4.setBounds(20,260,100,32);
		add(label4);
		
		choice1 = new Choice();
		choice1.setBounds(122, 260, 250, 32);
		choice1.addItem("Rojo");
		choice1.addItem("Amarillo");
		choice1.addItem("Azul");
		add(choice1);
		
		list1 =new List();
		list1.setBounds(20,295,350,90);
		list1.add("Manzana");
		list1.add("Pera");
		list1.add("Uva");
		list1.add("Fresa");
		list1.add("Jocote");
		list1.add("Mango");		
		add(list1);
		
		scrollbar1 = new Scrollbar();
		scrollbar1.setOrientation(Scrollbar.VERTICAL);
		scrollbar1.setBounds(460,295,16,80);
		add(scrollbar1);
		
		scrollbar2 = new Scrollbar();
		scrollbar2.setOrientation(Scrollbar.HORIZONTAL);
		scrollbar2.setBounds(375,375,90,16);
		add(scrollbar2);
		
		canvas1 = new Canvas();
		canvas1.setBounds(20, 400, 450, 100);
		canvas1.setBackground(Color.CYAN);
		add(canvas1);
		
		button1 = new Button("Botón 1");
		button1.setBounds(20,510,100,32);
		add(button1);
		
		button2 = new Button("Botón 2");
		button2.setBounds(370,510,100,32);
		add(button2);
	}
}
