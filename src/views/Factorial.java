package views;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Factorial extends WFrame {

    public Factorial(MainFrame intance) {
        super(intance);
        this.setTitle("Factorial");
        initComponents();
    }

    public void initComponents() {
        setSize(250, 250);
        setLayout(null);
        setLocationRelativeTo(this);

        Title = new Label("Calculate Factorial");
        Title.setBounds(60, 50, 150, 30);
        add(Title);

        SetNumber = new Label("Number: ");
        SetNumber.setBounds(20, 80, 50, 30);
        add(SetNumber);

        SetRes = new Label("Result: ");
        SetRes.setBounds(20, 130, 50, 30);
        add(SetRes);

        GetNumber = new TextField();
        GetNumber.setBounds(80, 80, 150, 30);
        add(GetNumber);

        GetRes = new TextField();
        GetRes.setEditable(false);
        GetRes.setBounds(80, 130, 150, 30);
        add(GetRes);

        Result = new Button("Result");
        Result.setBounds(20, 180, 50, 30);
        add(Result);

        Clean = new Button("Clean");
        Clean.setBounds(100, 180, 50, 30);
        add(Clean);

        Return = new Button("Return");
        Return.setBounds(180, 180, 50, 30);
        add(Return);

        Result.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ResultActionPerformed(evt);
            }
        });

        Clean.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                CleanActionPerformed(evt);
            }
        });

        Return.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ReturnActionPerformed(evt);
            }
        });
    }

    private void ResultActionPerformed(java.awt.event.ActionEvent evt) {
        int num, i;
        long factorial = 1;

        num = Integer.parseInt(GetNumber.getText());

        for (i = num; i > 0; i--) {
            factorial = factorial * i;
        }

        GetRes.setText(""+factorial);
    }
    
    private void CleanActionPerformed(java.awt.event.ActionEvent evt){
        GetNumber.setText("");
        GetRes.setText("");
    }
    
    private void ReturnActionPerformed(java.awt.event.ActionEvent evt){
        MainFrame mf = new MainFrame();
        mf.showForm();
        dispose();  
    }
    
    

    //Components
    private Frame Factorial;
    private Label Title, SetNumber, SetRes;
    private TextField GetNumber, GetRes;
    private Button Result, Clean, Return;

}
