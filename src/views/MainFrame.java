package views;

import java.awt.Button;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import misc.event.FormInterface;

public class MainFrame extends Frame implements FormInterface{
	/**
	 * author: William Sanchez
	 * Description: Formulario principal.
	 */
	private static final long serialVersionUID = -2483114542263271239L;
	private Button button1, button2, button3, button4,PrimeNumber,Factorial,Serie_fibonacci,n_ésimo;
	private MainFrame intance;
	public MainFrame() {
		initComponents();
                setTitle("Menu");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}
		});
	}
	@Override
	public void initComponents() {
		setLayout(null);
		setSize(390,200);
		setResizable(false);
		
		intance = this;
		button1 = new Button("Componentes");
		button1.setBounds(80,50,100,32);
		add(button1);
		

		button2 = new Button("Test");
		button2.setBounds(250,50,100,32);
		add(button2);
                
                PrimeNumber = new Button("Prime Number");
                PrimeNumber.setBounds(80, 100, 100, 32);
                add(PrimeNumber);
                
                Factorial = new Button("Factorial");
                Factorial.setBounds(250,100,100,32);
                add(Factorial);
                
                Serie_fibonacci = new Button("Serie_fibonacci");
                Serie_fibonacci.setBounds(80, 150, 100, 32);
                add(Serie_fibonacci);
                
                n_ésimo = new Button ("n_ésimo");
                n_ésimo.setBounds(250, 150, 100, 32);
                add(n_ésimo);
		
		button1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				ComponentsFrame cf = new ComponentsFrame(intance);
				cf.showForm();
			}
		});
		
		button2.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				TestFrame cf = new TestFrame(intance);
				cf.showForm();
			}
		});
                
                PrimeNumber.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				PrimeNumber PN = new PrimeNumber(intance);
				PN.showForm();
			}
		});
                
                Factorial.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Factorial F = new Factorial(intance);
				F.showForm();
			}
		});
                
                Serie_fibonacci.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Serie_fibonacci F = new Serie_fibonacci(intance);
				F.showForm();
			}
		}); 
                n_ésimo.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				n_ésimo n = new n_ésimo(intance);
				n.showForm();
			}
		});
	}
	@Override
	public void showForm() {
		setVisible(true);
		setLocationRelativeTo(null);
		toFront();
	}
	@Override
	public void clean() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void showForm(boolean maximize) {

	}
}