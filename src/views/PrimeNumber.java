package views;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class PrimeNumber extends WFrame {

    //Constructor, Class Principal
    public PrimeNumber(MainFrame intance) {
        super(intance);
        this.setTitle("PrimeNumber");
        initComponents();
    }

    //Construction initComponents
    public void initComponents() {
        setSize(250, 250);
        setLayout(null);
        setLocationRelativeTo(this);

        Title = new Label("Calculate Prime Number");
        Title.setBounds(60, 50, 150, 30);
        add(Title);

        SetNumber = new Label("Number: ");
        SetNumber.setBounds(20, 80, 50, 30);
        add(SetNumber);

        SetRes = new Label("Result: ");
        SetRes.setBounds(20, 130, 50, 30);
        add(SetRes);

        GetNumber = new TextField();
        GetNumber.setBounds(80, 80, 150, 30);
        add(GetNumber);

        GetRes = new TextField();
        GetRes.setEditable(false);
        GetRes.setBounds(80, 130, 150, 30);
        add(GetRes);

        Result = new Button("Result");
        Result.setBounds(20, 180, 50, 30);
        add(Result);

        Clean = new Button("Clean");
        Clean.setBounds(100, 180, 50, 30);
        add(Clean);

        Return = new Button("Return");
        Return.setBounds(180, 180, 50, 30);
        add(Return);

        Result.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ResultActionPerformed(evt);
            }
        });

        Clean.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                CleanActionPerformed(evt);
            }
        });

        Return.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ReturnActionPerformed(evt);
            }
        });
    }

    //Action Performed
    private void ResultActionPerformed(java.awt.event.ActionEvent evt) {
        int num, m = 2;
        boolean band = true;

        num = Integer.parseInt(GetNumber.getText());

        while ((band) && (m < num)) {
            if ((num % m) == 0) {
                band = false;
            } else {
                m = m + 1;
            }
        }

        if (band) {
            if (num <= 1) {
                GetRes.setText("  the number is not prime");
                GetRes.setForeground(Color.RED);
            } else {
                GetRes.setText("  the number is prime");
                GetRes.setForeground(Color.GREEN);
            }
        } else {
            GetRes.setText("  the number isn´t prime");
            GetRes.setForeground(Color.RED);
        }
    }

    //Clean
    private void CleanActionPerformed(java.awt.event.ActionEvent evt) {
        GetNumber.setText("");
        GetRes.setText("");
    }

    //Return
    private void ReturnActionPerformed(java.awt.event.ActionEvent evt) {
        MainFrame mf = new MainFrame();
        mf.showForm();
        dispose();
    }

    //Components
    private Frame Prime_Number;
    private Label Title, SetNumber, SetRes;
    private TextField GetNumber, GetRes;
    private Button Result, Clean, Return;
}
