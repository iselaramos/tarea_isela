package views;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import misc.event.FormInterface;

public class WFrame extends Frame implements FormInterface{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3044516451727926394L;
	Frame parentFrame;
	
	public WFrame(MainFrame parentFrame) {
		this.parentFrame = parentFrame;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if(parentFrame!=null) {
					parentFrame.setVisible(true);
				}
				dispose();
			}
		});
	}
	@Override
	public void initComponents() {
		
	}

	@Override
	public void showForm() {
		if(parentFrame!=null) {
			parentFrame.setVisible(false);
		}
		setVisible(true);
		setLocationRelativeTo(null);
		toFront();
		
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showForm(boolean maximize) {
		// TODO Auto-generated method stub
		
	}

}
