package views;

import java.awt.*;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextArea;

public class n_ésimo extends WFrame {

    public n_ésimo(MainFrame intance) {
        super(intance);
        this.setTitle("n_ésimo");
        initComponents();
        Result();
    }

    public void initComponents() {
        setSize(250, 250);
        setLayout(null);

        Title = new Label("enesimo");
        Title.setBounds(90, 40, 150, 30);
        add(Title);

        SetRes = new Label("Result: ");
        SetRes.setBounds(20, 70, 50, 30);
        add(SetRes);

        GetRes = new JTextArea();
        GetRes.setEditable(false);
        GetRes.setLineWrap(true);
        GetRes.setBounds(80, 70, 150, 100);
        add(GetRes);

        Return = new Button("Return");
        Return.setCursor(new Cursor(HAND_CURSOR));
        Return.setBounds(150, 180, 50, 30);
        add(Return);


        Return.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ReturnActionPerformed(evt);
            }
        });
    }

    private void Result() {
        long numero = 400;
        int fibo1, fibo2, i;

        fibo1 = 1;
        fibo2 = 1;

        GetRes.setText(fibo1 + ", ");
        for (i = 2; i <= numero; i++) {
            GetRes.setText(GetRes.getText() + fibo2 + ", ");
            fibo2 = fibo1 + fibo2;
            fibo1 = fibo2 - fibo1;
        }

    }

    // Clean The TextField
    private void CleanActionPerformed(java.awt.event.ActionEvent evt) {
        GetRes.setText("");
    }

    // Return to MainFrame
    private void ReturnActionPerformed(java.awt.event.ActionEvent evt) {
        MainFrame mf = new MainFrame();
        mf.showForm();
        dispose();
    }

    // Components
    private Frame n_esimo;
    private Label Title, SetNumber, SetRes;
    private JTextArea GetRes;
    private Button Return;

}
